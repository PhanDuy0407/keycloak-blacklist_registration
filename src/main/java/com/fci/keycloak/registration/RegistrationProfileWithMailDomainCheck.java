package com.fci.keycloak.registration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.ws.rs.core.MultivaluedMap;

import org.jboss.logging.Logger;
import org.keycloak.authentication.ValidationContext;
import org.keycloak.authentication.forms.RegistrationPage;
import org.keycloak.authentication.forms.RegistrationProfile;
import org.keycloak.events.Details;
import org.keycloak.events.Errors;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.services.messages.Messages;
import org.keycloak.services.validation.Validation;

import static org.keycloak.provider.ProviderConfigProperty.BOOLEAN_TYPE;


public class RegistrationProfileWithMailDomainCheck extends RegistrationProfile{

   public static final String PROVIDER_ID = "registration-blacklist-domain";
   private Connection connection;
   private List<String> domains;
   private final Logger logger = Logger.getLogger(RegistrationProfileWithMailDomainCheck.class.getName());

   // Default constructor
   public RegistrationProfileWithMailDomainCheck() {
      // Get db connection and blacklist domains on startup
      this.connection = getConnection();
      this.domains = getAllDomainMail(connection);
   }

   // Constructor for testing
   RegistrationProfileWithMailDomainCheck(List<String> domains){
      this.connection = null;
      this.domains = domains;
   }

   Connection getConnection() {
      try {
         String dataSourceLookup = "java:jboss/datasources/KeycloakDS";
         DataSource dataSource = (DataSource) new InitialContext().lookup(dataSourceLookup);
         return dataSource.getConnection();
      } catch (Exception e) {
         throw new RuntimeException("Failed to connect databases", e);
      } finally {
         close();
      }
   }

   public List<String> getAllDomainMail(Connection conn){
      if(conn != null){
         String sql = "SELECT FCI_BLACKLIST.MAIL_DOMAIN FROM FCI_BLACKLIST";

         try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            List<String> invalidDomains = new ArrayList<>();
            while (rs.next()){
               invalidDomains.add(rs.getString("MAIL_DOMAIN"));
            }
            return invalidDomains;
         } catch (SQLException e){
            logger.log(Logger.Level.WARN, e.getMessage());
            return new ArrayList<>();
         }
      }
      else return new ArrayList<>();
   }

   // Check if user's email in blacklist or not
   public boolean isValidEmail(String email, List<String> invalidDomains){
      boolean isValid = true;
      for (String domain : invalidDomains) {
         if (email==null || email.endsWith("@" + domain)) {
            isValid = false;
            break;
         }
      }
      return isValid;
   }

   public List<String> getDomains() {
      return domains;
   }

   @Override
   public String getDisplayType() {
      return "Profile Validation with Blacklist domain check";
   }

   @Override
   public String getId() {
      return PROVIDER_ID;
   }

   @Override
   public boolean isConfigurable() {
      return true;
   }

   @Override
   public List<ProviderConfigProperty> getConfigProperties() {
      List<ProviderConfigProperty> configList = new ArrayList<>();

      ProviderConfigProperty renewDomains = new ProviderConfigProperty();
      renewDomains.setType(BOOLEAN_TYPE);
      renewDomains.setName("renewDomains");
      renewDomains.setLabel("Renew connection");
      renewDomains.setHelpText("Set to TRUE when add some new domains to the list");
      renewDomains.setDefaultValue("false");

      configList.add(renewDomains);

      return configList;
   }

   @Override
   public void validate(ValidationContext context) {
      /*
        If "Renew connection" configuration set to TRUE (Administrator edited blacklist domains in db)
         - Get new blacklist domains
         - Set "Renew connection" to FALSE for next time
       */
      Map<String, String> config = context.getAuthenticatorConfig().getConfig();
      if(config.get("renewDomains").equalsIgnoreCase("true")){
         this.domains = getAllDomainMail(this.connection);
         config.replace("renewDomains","false");
         context.getAuthenticatorConfig().setConfig(config);
      }

      MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
      String email = formData.getFirst(Validation.FIELD_EMAIL);
      String eventError = Errors.INVALID_REGISTRATION;
      List<FormMessage> errors = new ArrayList<>();


      boolean emailDomainValid = isValidEmail(email, this.domains);
      if (!emailDomainValid) {
         errors.add(new FormMessage(RegistrationPage.FIELD_EMAIL, Messages.INVALID_EMAIL));
      }
      if (!errors.isEmpty()) {
         context.error(eventError);
         context.validationError(formData, errors);
      } else {
         context.success();
      }
   }

}
